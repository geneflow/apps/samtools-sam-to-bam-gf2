Samtools: SAM to BAM Geneflow2 App
==================================

Version: 1.10-01

This is a GeneFlow app that converts a SAM file to BAM format

Inputs
------

1. input: SAM file to convert.

Parameters
----------

1. output: BAM file output.
